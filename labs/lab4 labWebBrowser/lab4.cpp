//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "lab4.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::bugoClick(TObject *Sender)
{
	wb->URL = edURL->Text;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buBackClick(TObject *Sender)
{
    wb->GoBack();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	wb->GoForward();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::edURLKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if (Key == vkReturn) {
		wb->URL = edURL->Text;
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::wbDidFinishLoad(TObject *ASender)
{
    edURL->Text = wb->URL;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
	ShowMessage("Dugarovs");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buReClick(TObject *Sender)
{
    wb->Reload();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
    wb->Stop();
}
//---------------------------------------------------------------------------
