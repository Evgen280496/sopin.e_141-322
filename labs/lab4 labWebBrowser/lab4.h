//---------------------------------------------------------------------------

#ifndef lab4H
#define lab4H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.WebBrowser.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TToolBar *ToolBar2;
	TEdit *edURL;
	TButton *bugo;
	TWebBrowser *wb;
	TButton *buBack;
	TButton *Button2;
	TButton *buRe;
	TButton *Button4;
	TButton *Button5;
	void __fastcall bugoClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall edURLKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall wbDidFinishLoad(TObject *ASender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall buReClick(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
