//---------------------------------------------------------------------------

#ifndef paswdH
#define paswdH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.NumberBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TLabel *laCaption;
	TButton *buAbout;
	TLayout *ly;
	TEdit *edPassword;
	TButton *buPassword;
	TCheckBox *skLower;
	TNumberBox *edLength;
	TCheckBox *skUpper;
	TCheckBox *skNumber;
	TLabel *laLength;
	TCheckBox *skSpec;
	void __fastcall buPasswordClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
