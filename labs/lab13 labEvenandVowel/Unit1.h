//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *TMenu;
	TTabItem *TPlay;
	TTabItem *TFinish;
	TTabItem *THelp;
	TLayout *lyMenu;
	TLabel *Label1;
	TLabel *Label2;
	TGridPanelLayout *gplMenu;
	TButton *buPlay;
	TButton *buHelp;
	TButton *buExit;
	TGridPanelLayout *gplPlayButton;
	TButton *buYes;
	TButton *buNo;
	TLayout *lyPlayTop;
	TButton *buBackbuBack;
	TLabel *laTime;
	TLabel *laCorrect;
	TLayout *lyPlay;
	TGridPanelLayout *gplPlay;
	TLabel *Label5;
	TLabel *Label6;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TLabel *laBox1;
	TLabel *laBox2;
	TTimer *Timer1;
	TLayout *Layout1;
	TLabel *Label3;
	TLabel *laFinishCorrect;
	TLabel *laFinishWrong;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button2;
	TButton *Button3;
	TToolBar *ToolBar1;
	TButton *buBack;
	TLabel *Label8;
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall buHelpClick(TObject *Sender);
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall buBackbuBackClick(TObject *Sender);
	void __fastcall buYesClick(TObject *Sender);
	void __fastcall buNoClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
private:
	int FCountCorrect;
	int FCountWrong;
	bool FAnswerCorrect;
	double FTimeValue;
	void DoReset();
	void DoContinue();
	void DoAnswer(bool AValue);
	void DoFinish();
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
