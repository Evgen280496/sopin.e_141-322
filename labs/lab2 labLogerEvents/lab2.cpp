//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "lab2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ClearClick(TObject *Sender)
{
	meLine->Lines->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	meLine->Lines->Add("Button1.Clicked");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	meLine->Lines->Add(Button2->Name + ".Clicked");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonAllClick(TObject *Sender)
{
	meLine->Lines->Add(((TControl *)Sender)->Name + ".clicked");
}
//---------------------------------------------------------------------------
