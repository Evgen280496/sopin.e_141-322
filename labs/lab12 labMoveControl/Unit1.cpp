//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
//void __fastcall TForm1::RoundRect1MouseDown(TObject *Sender, TMouseButton Button,
//          TShiftState Shift, float X, float Y)
//{
//	FX = X;
//	FY = Y;
//	FIsDragging = true;
//	((TControl *)Sender)->BringToFront();
//	 ((TControl *)Sender)->Root->Captured = interface_cast<IControl>(Sender);
//	 if (dynamic_cast<TShape*>(Sender)) {
//		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Aquamarine;
//	 }
//
//}
////---------------------------------------------------------------------------
//void __fastcall TForm1::RoundRect1MouseEnter(TObject *Sender)
//{
//	if (dynamic_cast<TShape*>(Sender)) {
//		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Azure;
//	 }
//}
////---------------------------------------------------------------------------
//void __fastcall TForm1::RoundRect1MouseLeave(TObject *Sender)
//{
//	 if (dynamic_cast<TShape*>(Sender)) {
//		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Crimson;
//	 }
//}
////---------------------------------------------------------------------------
//void __fastcall TForm1::RoundRect1MouseMove(TObject *Sender, TShiftState Shift, float X,
//		  float Y)
//{
//	 if (FIsDragging && Shift.Contains(ssLeft)) {
//		((TControl *)Sender)->Position->X += X - FX;
//		((TControl *)Sender)->Position->Y += Y - FY;
//		if (dynamic_cast<TShape*>(Sender)) {
//			((TShape *)Sender)->Fill->Color = TAlphaColorRec::Aliceblue;
//		}
//	 }
//}
////---------------------------------------------------------------------------
//void __fastcall TForm1::RoundRect1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
//		  float X, float Y)
//{
//	FIsDragging = false;
//	((TControl *)Sender)->Root->Captured = 0;
//	if (dynamic_cast<TShape*>(Sender)) {
//		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Lightgoldenrodyellow;
//	 }
//}
//---------------------------------------------------------------------------
void __fastcall TForm1::Ellipse1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y)
{
   FX = X;
	FY = Y;
	FIsDragging = true;
	((TControl *)Sender)->BringToFront();
	 ((TControl *)Sender)->Root->Captured = interface_cast<IControl>(Sender);
	 if (dynamic_cast<TShape*>(Sender)) {
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Aquamarine;
	 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Ellipse1MouseEnter(TObject *Sender)
{
   if (dynamic_cast<TShape*>(Sender)) {
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Azure;
	 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Ellipse1MouseLeave(TObject *Sender)
{
    if (dynamic_cast<TShape*>(Sender)) {
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Crimson;
	 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Ellipse1MouseMove(TObject *Sender, TShiftState Shift, float X,
          float Y)
{
   if (FIsDragging && Shift.Contains(ssLeft)) {
		((TControl *)Sender)->Position->X += X - FX;
		((TControl *)Sender)->Position->Y += Y - FY;
		if (dynamic_cast<TShape*>(Sender)) {
			((TShape *)Sender)->Fill->Color = TAlphaColorRec::Aliceblue;
		}
	 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Ellipse1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y)
{
     FIsDragging = false;
	((TControl *)Sender)->Root->Captured = 0;
	if (dynamic_cast<TShape*>(Sender)) {
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Lightgoldenrodyellow;
	 }
}
//---------------------------------------------------------------------------

