//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop
#include "lab8.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	Randomize();
	DoReset();


}
//---------------------------------------------------------------------------
void Tfm::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	DoContinue();
}
void Tfm::DoContinue()
{
	laGood->Text = Format(L"����� = %d", ARRAYOFCONST((FCountCorrect)));
	laBad->Text = Format(L"������� = %d", ARRAYOFCONST((FCountWrong)));
	int a1 = Random(99);
	int a2 = Random(99);
	int res = a1 + a2;
	int sign = (Random(2) == 1) ? 1 : -1;
	int resNew = (Random(2) == 1) ? res : res + (Random(7) * sign);

	FAnswerCorrect = (res == resNew);
	laPrimer->Text = Format("%d + %d = %d",
		ARRAYOFCONST((a1, a2, resNew)));
}
void Tfm::DoAnswer(bool aValue)
{
	(aValue == FAnswerCorrect) ?
		 FCountCorrect++ : FCountWrong++;
	DoContinue();
}
void __fastcall Tfm::buRebootClick(TObject *Sender)
{
    DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDaClick(TObject *Sender)
{
    DoAnswer(true);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNetClick(TObject *Sender)
{
	DoAnswer(false);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buInfoClick(TObject *Sender)
{
    ShowMessage("Dugarov");
}
//---------------------------------------------------------------------------

