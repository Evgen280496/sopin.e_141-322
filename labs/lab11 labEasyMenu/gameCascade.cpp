//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "gameCascade.h"
#include "projInfo.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm1 *fm1;
//---------------------------------------------------------------------------
__fastcall Tfm1::Tfm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::Button1Click(TObject *Sender)
{
    tc->ActiveTab = tiPlay;
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::Button3Click(TObject *Sender)
{
	fmAbout->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::ButtonBackClick(TObject *Sender)
{
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::Button4Click(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::Button2Click(TObject *Sender)
{
	tc->ActiveTab = tiHelp;
}
//---------------------------------------------------------------------------

