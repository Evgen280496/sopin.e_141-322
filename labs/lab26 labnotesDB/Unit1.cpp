//---------------------------------------------------------------------------


#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tdm::FDConnectionBeforeConnect(TObject *Sender)
{
	FDConnection->Params->Values["Database"] =
#ifdef _Windows
		"..\\..\\" + cNameDB;
#else
			System::Ioutils::TPath::GetDocumentsPath() + PathDelim + cNameDB;
#endif
}
//---------------------------------------------------------------------------
void __fastcall Tdm::FDConnectionAfterConnect(TObject *Sender)
{
	FDConnection->ExecSQL(
		" CREATE TABLE IF NOT EXISTS [Notes]("
		" [CAPTION] VARCHAR(50) NOT NULL,"
		" [PRIORITY] SMALLINT NOT NULL,"
		" [DETAIL] VARCHAR(500))"
	);
	taNotes->Open();
}
//---------------------------------------------------------------------------

